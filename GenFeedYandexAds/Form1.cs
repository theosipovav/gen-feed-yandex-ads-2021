﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;


namespace GenFeedYandexAds
{
    public partial class Form1 : Form
    {

        string xml = "";

        public Form1()
        {
            InitializeComponent();



        }






        private void buttonOpenExcel_Click(object sender, EventArgs e)
        {

            string fileXls = "";
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Файлы Excel (*.xls, *.xlsx,) | *.xls; *.xlsx;";


                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    fileXls = openFileDialog.FileName;
                }
                else
                {
                    return;
                }
            }

            xml = "<?xml version='1.0' encoding='utf-8'?><feed version='1'><offers>";






            try
            {
                object rOnly = true;
                object SaveChanges = false;
                object MissingObj = System.Reflection.Missing.Value;
                Excel.Application app = new Excel.Application();
                Excel.Workbooks workbooks = null;
                Excel.Workbook workbook = null;
                Excel.Sheets sheets = null;
                workbooks = app.Workbooks;
                workbook = workbooks.Open(fileXls, MissingObj, rOnly, MissingObj, MissingObj, MissingObj, MissingObj, MissingObj, MissingObj, MissingObj, MissingObj, MissingObj, MissingObj, MissingObj, MissingObj);
                sheets = workbook.Sheets;   // Получение всех страниц докуента
                foreach (Excel.Worksheet worksheet in sheets)
                {
                    Excel.Range UsedRange = worksheet.UsedRange;    // Получаем диапазон используемых на странице ячеек
                    Excel.Range urRows = UsedRange.Rows;    // Получаем строки в используемом диапазоне
                    Excel.Range urColums = UsedRange.Columns;   // Получаем столбцы в используемом диапазоне
                    int RowsCount = urRows.Count;   // Количества строк и столбцов
                    int ColumnsCount = urColums.Count;
                    for (int i = 3; i <= RowsCount; i++)
                    {


                        string id = "";
                        string contact_method = "any";
                        string phone = "";
                        string title = "";
                        string description = "";
                        string condition = "new";
                        string category = "";
                        List<string> images = new List<string>();
                        string price = "";



                        string address = textBoxAddress.Text;
                        string video = "";


                        for (int j = 1; j <= ColumnsCount; j++)
                        {
                            Excel.Range CellRange = UsedRange.Cells[i, j];
                            string CellText = (CellRange == null || CellRange.Value2 == null) ? null : (CellRange as Excel.Range).Value2.ToString();   // Получение текста ячейки




                            if (CellText != null)
                            {
                                CellText = CellText.Replace("\"", "&quot;");
                                CellText = CellText.Replace("&", "&amp;");
                                CellText = CellText.Replace(">", "&gt;");
                                CellText = CellText.Replace("<", "&lt;");
                                CellText = CellText.Replace("'", "&apos;");
                                switch (j)
                                {
                                    case 1:
                                        id = CellText;
                                        id = id.TrimStart(' ');
                                        id = id.TrimEnd(' ');
                                        break;
                                    case 2:
                                        contact_method = CellText;
                                        contact_method = contact_method.TrimStart(' ');
                                        contact_method = contact_method.TrimEnd(' ');
                                        break;
                                    case 3:
                                        phone = CellText;
                                        phone = phone.TrimStart(' ');
                                        phone = phone.TrimEnd(' ');
                                        break;
                                    case 4:
                                        title = CellText;
                                        break;
                                    case 5:
                                        description += String.Format("Тип кондиционера: {0} \n", CellText);
                                        break;
                                    case 6:
                                        description += String.Format("Производитель: {0} \n", CellText);
                                        break;
                                    case 7:
                                        description += String.Format("Обслуживаемая площадь: {0} \n", CellText);
                                        break;
                                    case 8:
                                        description += String.Format("Инверторный кондиционер: {0} \n", CellText);
                                        break;
                                    case 9:
                                        condition = CellText;
                                        condition = condition.TrimStart(' ');
                                        condition = condition.TrimEnd(' ');
                                        break;
                                    case 10:
                                        category = CellText;
                                        break;
                                    case 11:
                                        foreach (string image in CellText.Split(','))
                                        {
                                            images.Add(image.Replace(" ", ""));
                                        }
                                        break;
                                    case 12:
                                        price = CellText;
                                        break;
                                    default:
                                        break;

                                }
                            }
                        }


                        xml += String.Format("<offer>");
                        xml += String.Format("<id>{0}</id>", id);
                        xml += String.Format("<seller>");
                        xml += String.Format("<contacts>");
                        xml += String.Format("<phone>{0}</phone>", phone);
                        xml += String.Format("<contact-method>{0}</contact-method>", contact_method);
                        xml += String.Format("</contacts>");
                        xml += String.Format("<locations>");
                        xml += String.Format("<location>");
                        xml += String.Format("<address>{0}</address>", address);
                        xml += String.Format("</location>");
                        xml += String.Format("</locations>");
                        xml += String.Format("</seller>");
                        xml += String.Format("<title>{0}</title>", title);
                        xml += String.Format("<description>{0}</description>", description);
                        xml += String.Format("<condition>{0}</condition>", condition);
                        xml += String.Format("<category>{0}</category>", category);
                        xml += String.Format("<images>");
                        foreach (string image in images)
                        {
                            xml += String.Format("<image>{0}</image>", image);
                        }
                        xml += String.Format("</images>");
                        //xml += String.Format("<video>{0}</video>", video);
                        xml += String.Format("<price>{0}</price>", price);
                        xml += String.Format("</offer>");
                    }
                    // Очистка неуправляемых ресурсов на каждой итерации
                    if (urRows != null) Marshal.ReleaseComObject(urRows);
                    if (urColums != null) Marshal.ReleaseComObject(urColums);
                    if (UsedRange != null) Marshal.ReleaseComObject(UsedRange);
                    if (worksheet != null) Marshal.ReleaseComObject(worksheet);



                }
            }
            catch (Exception theException)
            {
                String errorMessage;
                errorMessage = "Error: ";
                errorMessage = String.Concat(errorMessage, theException.Message);
                errorMessage = String.Concat(errorMessage, " Line: ");
                errorMessage = String.Concat(errorMessage, theException.Source);
                MessageBox.Show(errorMessage, "Error");
            }


            xml += "</offers></feed>";






            MessageBox.Show("Файл Excel загружен", "Успешно!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            

        }

        private void buttonSaveToXml_Click(object sender, EventArgs e)
        {

            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "Файлы Excel (*.xml) | *.xml;";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    using (StreamWriter writer = new StreamWriter(new FileStream(saveFileDialog.FileName, FileMode.OpenOrCreate), Encoding.GetEncoding("utf-8")))
                    {
                        writer.Write(xml);
                    }
                }
                else
                {
                    return;
                }

            }
        }
    }
}
